<?php

declare(strict_types = 1);

namespace Drupal\ckeditor_component_library\Plugin\EmbeddedContent;

use Drupal\ckeditor5_embedded_content\EmbeddedContentInterface;
use Drupal\ckeditor5_embedded_content\EmbeddedContentPluginBase;
use Drupal\component_library\ComponentLibraryVariantInterface;
use Drupal\component_library\Entity\ComponentLibraryPattern;
use Drupal\component_library\Form\ComponentLibraryVariantForm;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @EmbeddedContent(
 *   id = "component_library",
 *   label = @Translation("Component Library"),
 *   description = @Translation("Embeds a rendered component_library pattern or variant."),
 *   deriver = "\Drupal\ckeditor_component_library\Plugin\Derivative\CkeditorComponentLibraryEmbedDeriver"
 * )
 */
final class ComponentLibrary extends EmbeddedContentPluginBase implements EmbeddedContentInterface, ContainerFactoryPluginInterface {

  public const VARIANT_CONFIG_KEY = 'component_library_variant';

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly LoggerInterface $logger
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('logger.factory')->get('ckeditor_component_library')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return \array_merge(
      parent::defaultConfiguration(),
      $this->getPluginDefinition()['pattern_default_config'],
      [self::VARIANT_CONFIG_KEY => '']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    /** @var \Drupal\component_library\Entity\ComponentLibraryVariant $variant */
    $variant_id = $this->getConfiguration()[self::VARIANT_CONFIG_KEY];
    $variant = $this->entityTypeManager->getStorage('component_library_variant')
      ->load($variant_id);
    if ($variant === NULL) {
      $this->logger->warning('Component library variant %variant missing for CKEditor embedded content.', [
        '%variant' => $variant_id,
      ]);
      return [];
    }
    \assert($variant instanceof ComponentLibraryVariantInterface);
    $template = $variant->get('template');

    $build = [
      '#theme' => 'ckeditor_component_library_embed',
      // @todo Figure out how to not use our own theme hook here and instead use
      //   '#type' => 'component_library_component'. This isn't straight forward
      //   because the pattern ID is pulled from $info in
      //   template_preprocess_component_library_component()
      //   and without it, rendering won't work.
      '#content' => [
        '#type' => 'inline_template',
        '#template' => $template,
        '#context' => $this->configToTemplateContext(),
      ],
      '#plugin_definition' => $this->getPluginDefinition(),
    ];
    $cache_metadata = new CacheableMetadata();
    $cache_metadata->addCacheableDependency($variant);
    $cache_metadata->addCacheableDependency($variant->getPatternEntity());
    $cache_metadata->applyTo($build);
    return $build;
  }

  /**
   * Ensures data integrity in template context based on plugin configuration.
   */
  private function configToTemplateContext(): array {
    $context = $this->configuration;
    foreach ($context as &$value) {

      // Empty text values may have unexpected results during rendering of twig
      // templates. To ensure consistency with the code for previews on the
      // ComponentLibraryVarianForm and proper rendering of previews, such
      // values are explicitly converted to NULL here.
      if ($value === '') {
        $value = NULL;
      }

      // @todo For more complex field types like for example a date or a color
      //   field, we may need a more sophisticated way of converting stored
      //   values into the structure that is needed in the template context. We
      //   may introduce a plugin type for this which will provide the form
      //   element and take care of casting the value into what is needed.
    }
    return ComponentLibraryVariantForm::convertYamlPlaceholders($context);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $enabled_components = $this->configFactory->get('ckeditor_component_library.settings')->get('enabled_components');
    $plugin_definition = $this->getPluginDefinition();
    $pattern_id = $plugin_definition['component_library_pattern'];
    $plugin_config = $this->getConfiguration();
    $variants = $this->getComponentLibraryPatternEntity()?->getVariants();
    $default_variant = $this->getDefaultVariantName();

    if (!empty($variants) && \count($variants) > 1) {
      $form['component_library_variant'] = [
        '#type' => 'select',
        '#options' => \array_map(
          static fn (ComponentLibraryVariantInterface $variant) => $variant->label(),
          $variants
        ),
        '#title' => $this->t('Variant'),
        '#sort_options' => TRUE,
        '#default_value' => $plugin_config[self::VARIANT_CONFIG_KEY] ?? $default_variant,
      ];
    }
    else {
      $form[self::VARIANT_CONFIG_KEY] = [
        '#type' => 'value',
        '#value' => $default_variant,
      ];
    }

    foreach ($plugin_definition['pattern_default_config'] as $field => $default) {
      $field_settings = !empty($enabled_components[$pattern_id])
        ? \json_decode($enabled_components[$pattern_id], TRUE)
        : [];
      $form[$field] = [
        '#title' => $field,
        '#default_value' => $plugin_config[$field] ?? $default ?? '',
      ];

      if (empty($field_settings[$field])) {
        $form[$field]['#type'] = self::getDefaultFormElementType($default);
      }
      else {
        $this->applyCustomFieldDefinitions($form[$field], $field_settings[$field]);
      }
    }
    return $form;
  }

  /**
   * Sets additional properties as defined on the component configuration form.
   */
  private function applyCustomFieldDefinitions(array &$element, array $field_definitions): void {
    foreach ($field_definitions as $fapi_property => $property_value) {
      $element[$fapi_property] = $property_value;
    }
  }

  /**
   * Determines a default fapi element typy considering given default value.
   */
  public static function getDefaultFormElementType(mixed $default_value): string {
    if (empty($default_value)) {
      // @todo Should the default fill value here be exposed in config?
      $type = 'textfield';
    }
    else {
      if (\is_scalar($default_value)) {
        $type = \mb_strlen((string) $default_value) <= 128 ? 'textfield' : 'textarea';
      }
      else {
        $type = 'textarea';
      }
    }
    return $type;
  }

  /**
   * Loads the pattern entity based on the id in plugin definition.
   */
  private function getComponentLibraryPatternEntity(): ?ComponentLibraryPattern {
    /** @var \Drupal\component_library\Entity\ComponentLibraryPattern|NULL $pattern */
    $pattern = $this->entityTypeManager->getStorage('component_library_pattern')
      ->load($this->getPluginDefinition()['component_library_pattern']);
    return $pattern;
  }

  /**
   * Gets the default variant ID for the plugins pattern.
   */
  private function getDefaultVariantName(): string {
    return $this->getPluginDefinition()['component_library_pattern'] . '__default';
  }

}
