<?php

declare(strict_types = 1);

namespace Drupal\ckeditor_component_library\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides embedded content definitions for component_library variants.
 *
 * @see \Drupal\ckeditor_component_library\Plugin\EmbeddedContent\ComponentLibrary
 */
final class CkeditorComponentLibraryEmbedDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Constructs new CkeditorComponentLibraryEmbed.
   */
  public function __construct(private readonly EntityTypeManagerInterface $entityTypeManager, private readonly ConfigFactoryInterface $configFactory) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $config = $this->configFactory->get('ckeditor_component_library.settings');
    $enabled_components = $config->get('enabled_components') ?: [];
    if (empty($enabled_components)) {
      return [];
    }

    /** @var \Drupal\component_library\Entity\ComponentLibraryPattern[] $patterns */
    $patterns = $this->entityTypeManager->getStorage('component_library_pattern')
      ->loadMultiple(\array_keys($enabled_components));

    foreach ($patterns as $id => $pattern) {
      $label = \implode(': ', [
        // @todo does this have to be translatable?
        'Component Library',
        $pattern->label(),
      ]);
      $this->derivatives[$id] = $base_plugin_definition;
      $this->derivatives[$id]['admin_label'] = $label;
      $this->derivatives[$id]['label'] = $label;
      $this->derivatives[$id]['component_library_pattern'] = $pattern->id();
      $this->derivatives[$id]['pattern_default_config'] = $pattern->getFields();
      $this->derivatives[$id]['config_dependencies']['config'] = [$pattern->getConfigDependencyName()];
    }

    return $this->derivatives;
  }

}
