<?php

declare(strict_types = 1);

namespace Drupal\ckeditor_component_library\Form;

use Drupal\ckeditor_component_library\Plugin\EmbeddedContent\ComponentLibrary;
use Drupal\component_library\Entity\ComponentLibraryPattern;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\Serialization\Yaml;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Ckeditor component library settings for this site.
 */
final class ComponentConfigurationForm extends ConfigFormBase {

  public const CONFIG_FORM_SETTINGS_KEY = 'config_form_settings';
  public const PATTERN_ENABLED_KEY = 'enabled';

  /**
   * Constructs a ComponentConfigurationForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, private readonly EntityTypeManagerInterface $entityTypeManager, private readonly CachedDiscoveryClearerInterface $pluginCache) {
    parent::__construct($config_factory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager'),
      $container->get('plugin.cache_clearer'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'ckeditor_component_library_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['ckeditor_component_library.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\component_library\Entity\ComponentLibraryPattern[] $patterns */
    $patterns = $this->entityTypeManager->getStorage('component_library_pattern')
      ->loadMultiple();
    $config = $this->config('ckeditor_component_library.settings');
    $enabled_components = $config->get('enabled_components');

    if (empty($patterns)) {
      return [
        '#markup' => $this->t('There are no component library patterns yet. Patterns can only be enabled for embedding once they were created.'),
      ];
    }

    $form['enabled_components'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    foreach ($patterns as $pattern) {
      if (!$pattern->status() || $pattern->id() === NULL) {
        continue;
      }

      $pattern_id = $pattern->id();
      $pattern_is_enabled = \array_key_exists($pattern_id, $enabled_components);
      $form['enabled_components'][$pattern_id] = [
        '#type' => 'details',
        '#title' => $pattern->label(),
        '#open' => $pattern_is_enabled,
        '#tree' => TRUE,
      ];

      $form['enabled_components'][$pattern_id][self::PATTERN_ENABLED_KEY] = [
        '#title' => $this->t('Allow embedding of %pattern components', ['%pattern' => $pattern->label()]),
        '#type' => 'checkbox',
        '#default_value' => $pattern_is_enabled,
      ];

      // Place the field config below the variants to enable, as settings a
      // fîeld mapping without enabling variants is useless.
      // @todo enable linting here, see
      //   https://www.drupal.org/project/codemirror_editor/issues/3174335
      //   https://codemirror.net/3/doc/manual.html#addon_lint
      $form['enabled_components'][$pattern_id][self::CONFIG_FORM_SETTINGS_KEY] = [
        '#type' => 'codemirror',
        '#codemirror' => [
          'foldGutter' => TRUE,
          'lineNumbers' => TRUE,
          'lineWrapping' => TRUE,
          'toolbar' => FALSE,
          'mode' => 'application/json',
          'lint' => TRUE,
        ],
        '#title' => $this->t('Embed Config Form Settings'),
        '#default_value' => $pattern_is_enabled
          ? $enabled_components[$pattern_id]
          : $this->getPluginConfigFormDefault($pattern),
        '#element_validate' => ['::validateEmbedConfigFormSettings'],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('ckeditor_component_library.settings')
      ->set('enabled_components', $this->buildEnabledComponentsValue($form_state))
      ->save();

    // The plugin deriver is providing derivatives based on the config saved
    // above, so we need to clear the cached definitions after changing the
    // config.
    $this->pluginCache->clearCachedDefinitions();
    parent::submitForm($form, $form_state);
  }

  /**
   * Ensures that only actually enabled components get stored.
   */
  private function buildEnabledComponentsValue(FormStateInterface $form_state): array {
    $submitted = $form_state->getValue('enabled_components');
    $relevant_values = [];

    // Filter out disabled patterns before storing.
    foreach ($submitted as $pattern_id => $values) {
      if (!empty($values[self::PATTERN_ENABLED_KEY])) {
        $relevant_values[$pattern_id] = $values[self::CONFIG_FORM_SETTINGS_KEY];
      }
    }
    return $relevant_values;
  }

  /**
   * Provides default for the field types used on the embed config form.
   */
  private function getPluginConfigFormDefault(ComponentLibraryPattern $pattern): ?string {
    $pattern_data = $pattern->get('data');
    if (empty($pattern_data)) {
      return NULL;
    }

    $pattern_data = Yaml::decode($pattern_data);
    foreach ($pattern_data as $key => $default) {
      $pattern_data[$key] = ['#type' => ComponentLibrary::getDefaultFormElementType($default)];
    }
    return \json_encode($pattern_data, \JSON_PRETTY_PRINT) ?: NULL;
  }

  /**
   * Validates JSON in embed form configuration fields.
   */
  public function validateEmbedConfigFormSettings(array $element, FormStateInterface $form_state): void {
    $user_input = $form_state->getUserInput();
    $pattern_id = $element['#parents'][1];
    // Check if the JSON entered inside the element to be validated is valid.
    if (!empty($user_input['enabled_components'][$pattern_id]) && \json_decode($user_input['enabled_components'][$pattern_id][self::CONFIG_FORM_SETTINGS_KEY]) === NULL) {
      // @phpstan-ignore-next-line
      $form_state->setError($element, $this->t('The entered value for %field is not valid JSON and can not be saved.', ['%field' => $pattern_id . ': Embed Config Form Settings']));
    }
  }

}
