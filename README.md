## Introduction

Based on [component_library](https://www.drupal.org/project/component_library)
and [ckeditor5_embedded_content](https://www.drupal.org/project/ckeditor5_embedded_content),
this module here will allow you to embed your Component Library pattern variants
inside ckeditor5 content.

### Features

- On a per pattern base configuration of the enabled embeds and their properties
exposed on the embed form
- Embedding of Component Library pattern variants inside ckeditor5

## Requirements

Enabled the following modules:
- component_library
- ckeditor_embedded_content
- ui_patterns

Create at least one component library patterns and at make sure there's at least
one variant of it.

## Installation

Install this Drupal module as you would normally install a contributed
Drupal module. For more information, visit:
https://www.drupal.org/documentation/install/modules-themes/modules-8

## Configuration

- On any text format using ckeditor5 as editor, enable the `Embedded Content`
toolbar button and corresponding filter and save the format.
- Navigate to `/admin/structure/component-library/ckeditor-embeds` to
  - Enable the patterns you want to enable for embedding in ckeditor
  - Configure the form elements exposed on the embed form, see detail below
- Save the form and try inserting a component library pattern/variant
combination of your choice inside ckeditor bia the `Embedded Content` button.

### Configuring the embed form elements

The embed form exposes a form element for each property of the component library
pattern you want to embed.

By default, each field is a textfield, but you can change that at
`/admin/structure/component-library/ckeditor-embeds` by changing the config of
each data property in a pattern's `Embed Config Form Settings` field.

The keys in this field correspond to the data properties of the pattern, while
the values consists of an array (JSON object) of Drupal Form API (FAPI)
properties and corresponding values. This allows things like changing of the
exposed field widget type, using `"#type": "value` or `"#acesss": "FALSE` to
hide the element from the form.

#### Example configuration

Component library pattern `Preview data` (data that can be accessed in template):

```yaml
link_text: Call to action
url: https://www.example.com
html_tag: a
```

Component library variant template:

```html
<{{ html_tag }}{{ button_attributes }} href="{{ url }}" style="background-color: red; color: white; border: 1px solid grey; padding: 10px 20px; border-radius: 50px; width: fit-content;">
  {{link_text|raw}}
</{{ html_tag }}>
```

`Embed Config Form Settings` for the pattern on the `/admin/structure/component-library/ckeditor-embeds`
which will disallow manually changing the `html_tag` property as that may cause
issues:

```json
{
    "link_text": {
        "#type": "textfield"
    },
    "url": {
        "#type": "url"
    },
    "hhml_tag": {
        "#type": "value"
    }
}
```
